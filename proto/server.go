package proto

import (
	"crypto/rand"
	"fmt"
	"net"
	"time"
)

/* wait for connection */
func (t *Transfer) Accept() error {
	server, err := net.Listen("tcp", fmt.Sprintf(":%d", DefaultPort))
	if err != nil {
		return err
	}
	conn, err = server.Accept()
	if err != nil {
		return err
	}
	t.TimeStamp = time.Now().Unix()

	data := make([]byte, 1024)
	/* receive public key from client */
	conn.Read(data)
	publickey := LoadPublicKey(data)

	/* send AES key to client */
	rand.Read(t.AES128[:])
	conn.Write(EncryptRSA(t.AES128[:], publickey))
	return nil
}
